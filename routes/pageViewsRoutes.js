const express = require("express");
const { getPageViews } = require("./../controller/pageViews");

const router = express.Router();

router.get("/", getPageViews);

module.exports = router;
