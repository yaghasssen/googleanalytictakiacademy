const express = require("express");
const { getRefferenceUsers } = require("../controller/RefferenceData");

const router = express.Router();

router.get("/refference", getRefferenceUsers);

module.exports = router;
