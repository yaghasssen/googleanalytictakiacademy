const { getData } = require("./../libraries/gAnalytics");
const MostViews = require("./../models/mostPageViews");
const getPageViews = (req, res) => {
  Promise.all(getData("pageviews", "180daysAgo", "today"))
    .then(async (data) => {
      // flatten list of objects into one object
      const body = {};
      Object.values(data).forEach((value) => {
        Object.keys(value).forEach(async (key) => {
          body[key] = value[key];
        });
      });
      const AllData = await MostViews.find({});
      // console.log(body);
      res.send({ data: body, AllData: AllData });
      console.log("Done");
    })
    .catch((err) => {
      console.log("Error:");
      console.log(err);
      res.send({ status: "Error getting a metric", message: `${err}` });
      console.log("Done");
    });
};

module.exports = { getPageViews };
