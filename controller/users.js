const { getUsresData } = require("./../libraries/gAnalytics");
const Ganalytics = require("./../models/statistic");
const getUsers = (req, res) => {
  Promise.all(getUsresData("newUsers", "30daysAgo", "today"))
    .then(async (data) => {
      // flatten list of objects into one object
      const body = {};
      Object.values(data).forEach((value) => {
        Object.keys(value).forEach(async (key) => {
          body[key] = value[key];
        });
      });
      const AllData = await Ganalytics.find({});
      res.send({ data: body, AllData: AllData });
      console.log("Done");
    })
    .catch((err) => {
      console.log("Error:");
      console.log(err);
      res.send({ status: "Error getting a metric", message: `${err}` });
      console.log("Done");
    });
};

module.exports = { getUsers };
