const Ganalytics = require("./../models/statistic");
const MostViews = require("./../models/mostPageViews");
const RefferenceData = require("./../models/RefferenceVisit");

const { google } = require("googleapis");
const analytics = google.analytics("v3");
const clientEmail = process.env.CLIENT_EMAIL;
const privateKey = process.env.PRIVATE_KEY.replace(new RegExp("\\\\n"), "\n");
const scopes = process.env.SCOPES;
const viewId = process.env.VIEW_ID;
const jwt = new google.auth.JWT({
  email: clientEmail,
  key: privateKey,
  scopes,
});

// gAnalytics.js
async function getMetric(metric, startDate, endDate) {
  await setTimeout[Object.getOwnPropertySymbols(setTimeout)[0]](
    Math.trunc(1000 * Math.random())
  );
  await jwt.authorize();
  const files = [];
  const result = await analytics.data.ga.get({
    auth: jwt,
    ids: `ga:${viewId}`,
    "start-date": "180daysAgo",
    "end-date": "today",
    metrics: "ga:pageviews",
    dimensions: "ga:date,ga:pagePath",
  });

  let path = "";
  let index = 0;
  let year = "";
  let month = "";
  let day = "";
  let DATE = "";
  let obj = {};

  let PathPage = "";
  result.data.rows.map((el) => {
    path = el.toLocaleString();
    index = path.indexOf(",");
    PathPage = path.slice(0, index);
    year = PathPage.slice(0, 4);
    month = PathPage.slice(4, 6);
    day = PathPage.slice(6, 8);
    DATE = `${year}-${month}-${day}`;
    PathPage = path.slice(index + 1, path.length);
    let DateView = new Date(DATE);
    obj = { DateView, PathPage };
    files.push(obj);
  });

  await MostViews.create({
    mostPageView: files,
  });
  const res = {};
  res[metric] = {
    value: parseInt(result.data.totalsForAllResults[metric], 10),
    start: "180daysAgo",
    end: "today",
  };
  await setTimeout[Object.getOwnPropertySymbols(setTimeout)[0]](
    Math.trunc(1000 * Math.random())
  );
  return res;
}
async function getUsers(metric, startDate, endDate) {
  await setTimeout[Object.getOwnPropertySymbols(setTimeout)[0]](
    Math.trunc(1000 * Math.random())
  );
  await jwt.authorize();
  let date = "";

  const result = await analytics.data.ga.get({
    auth: jwt,
    ids: `ga:${viewId}`,
    "start-date": "180daysAgo",
    "end-date": "today",
    metrics: "ga:newUsers",
    dimensions: "ga:date",
  });

  date = result.data.rows[result.data.rows.length - 1].toLocaleString();

  let index = 0;
  let newString = "";
  let year = "";
  let month = "";
  let day = "";
  if (date.indexOf(",") != -1) {
    index = date.indexOf(",");
    newString = date.slice(0, index);
    year = newString.slice(0, 4);
    month = newString.slice(4, 6);
    day = newString.slice(6, 8);
  }
  let finalDate = `${year}-${month}-${day}`;
  const DATE = new Date(finalDate);
  await Ganalytics.create({
    numberUser: parseInt(result.data.totalsForAllResults["ga:newUsers"], 10),
    dateCatchUser: DATE,
  });
  const res = {};
  res[metric] = {
    value: parseInt(result.data.totalsForAllResults["ga:newUsers"], 10),
    start: "180daysAgo",
    end: "today",
  };
  await setTimeout[Object.getOwnPropertySymbols(setTimeout)[0]](
    Math.trunc(1000 * Math.random())
  );
  return res;
}
async function getReference(metric, startDate, endDate) {
  await setTimeout[Object.getOwnPropertySymbols(setTimeout)[0]](
    Math.trunc(1000 * Math.random())
  );
  await jwt.authorize();

  const result = await analytics.data.ga.get({
    auth: jwt,
    ids: `ga:${viewId}`,
    "start-date": "180daysAgo",
    "end-date": "today",
    metrics: "ga:newUsers",
    dimensions:
      "ga:date,ga:source,ga:medium,ga:channelGrouping,ga:referralPath,ga:socialNetwork",
  });
  let files = [];

  let path = "";
  let index = 0;

  let year = "";
  let month = "";
  let day = "";
  let DATE = "";
  let obj = {};

  let FromWhereAccess = "";
  result.data.rows.map((el) => {
    path = el.toLocaleString();
    index = path.indexOf(",");
    PathPage = path.slice(0, index);
    year = PathPage.slice(0, 4);
    month = PathPage.slice(4, 6);
    day = PathPage.slice(6, 8);
    DATE = `${year}-${month}-${day} `;
    FromWhereAccess = path.slice(index + 1, path.length);
    let DateAccess = new Date(DATE);
    obj = { DateAccess, FromWhereAccess };
    files.push(obj);
  });
  await RefferenceData.create({
    RefferenceData: files,
  });
  const res = {};
  res[metric] = {
    value: parseInt(result.data.totalsForAllResults["ga:newUsers"], 10),
    start: "180daysAgo",
    end: "today",
  };
  await setTimeout[Object.getOwnPropertySymbols(setTimeout)[0]](
    Math.trunc(1000 * Math.random())
  );
  return res;
}

function parseMetric(metric) {
  let cleanMetric = metric;
  if (!cleanMetric.startsWith("ga:")) {
    cleanMetric = `ga:${cleanMetric}`;
  }
  return cleanMetric;
}

function getData(startDate = "", endDate = " ") {
  // ensure all metrics have ga:
  const results = [];

  const metric = parseMetric("pageviews");
  results.push(getMetric(metric, startDate, endDate));
  return results;
}
function getUsresData(startDate = "", endDate = " ") {
  // ensure all metrics have ga:
  const results = [];
  const metric = parseMetric("newUsers");
  results.push(getUsers(metric, startDate, endDate));

  return results;
}
function getRefferenceData(startDate = "", endDate = " ") {
  // ensure all metrics have ga:
  const results = [];
  const metric = parseMetric("newUsers");
  results.push(getReference(metric, startDate, endDate));

  return results;
}

module.exports = { getData, getUsresData, getRefferenceData };
