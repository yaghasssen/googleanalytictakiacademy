const express = require("express");
const app = express();
const cors = require("cors");
app.use(cors());
const usersRouter = require("./routes/users");
const pageViewsRouter = require("./routes/pageViewsRoutes");
const UsersRefferenceRouter = require("./routes/UsersReff");

app.use("/api", usersRouter, pageViewsRouter, UsersRefferenceRouter);

module.exports = app;
