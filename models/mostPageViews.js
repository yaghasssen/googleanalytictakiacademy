const mongoose = require("mongoose");

const MostPageViewsSchema = new mongoose.Schema(
  {
    mostPageView: {},
  },
  { timestamps: true }
);
const MostViews = mongoose.model("MostViews", MostPageViewsSchema);

module.exports = MostViews;
