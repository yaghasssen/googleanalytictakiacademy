const mongoose = require("mongoose");

const gAnalyticsSchema = new mongoose.Schema(
  {
    numberUser: { type: Number },
    dateCatchUser: { type: String },
  },
  { timestamps: true }
);
const Ganalytics = mongoose.model("Ganalytics", gAnalyticsSchema);

module.exports = Ganalytics;
