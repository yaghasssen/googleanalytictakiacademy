const mongoose = require("mongoose");

const RefferenceVisitSchema = new mongoose.Schema(
  {
    RefferenceData: {},
  },
  { timestamps: true }
);
const RefferenceData = mongoose.model("RefferenceData", RefferenceVisitSchema);

module.exports = RefferenceData;
